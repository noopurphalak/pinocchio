"""pinocchio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from cart import views as cart_views
from rest_framework import routers


if settings.DEBUG:
    router = routers.DefaultRouter()
else:
    router = routers.SimpleRouter()

router.register("cart/pizzas", cart_views.PizzaViewSet)
router.register("cart/toppings", cart_views.ToppingViewSet)
router.register("cart/pastas", cart_views.PastaViewSet)
router.register("cart/salads", cart_views.SaladViewSet)
router.register("cart/platters", cart_views.PlatterViewSet)
router.register("cart/pizzarates", cart_views.PizzaRateViewSet)
router.register("cart/platterrates", cart_views.PlatterRateViewSet)


urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
