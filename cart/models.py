import uuid

from django.core import validators as django_validators
from django.db import models

from autoslug import AutoSlugField


class AbstractTimeStampMixin(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Pizza(AbstractTimeStampMixin):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from="name", always_update=False, unique=True, db_index=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Topping(AbstractTimeStampMixin):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from="name", always_update=False, unique=True, db_index=True)

    def __str__(self):
        return self.name


class PizzaRateManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(
                is_active=True
            )
        )


class PizzaRate(AbstractTimeStampMixin):
    SMALL = 'small'
    LARGE = 'large'
    PIZZA_TYPE_CHOICES = (
        (SMALL, 'Small'),
        (LARGE, 'Large')
    )
    pizza = models.ForeignKey(Pizza, related_name='rates', on_delete=models.CASCADE)
    pizza_type = models.CharField(max_length=100, choices=PIZZA_TYPE_CHOICES, default=SMALL)
    no_of_toppings = models.IntegerField(
        default=0,
        validators=[
            django_validators.MinValueValidator(0),
            django_validators.MaxValueValidator(4)
        ]
    )
    rate = models.DecimalField(max_digits=5, decimal_places=2)
    is_active = models.BooleanField(default=True)

    objects = PizzaRateManager()
    all_objects = models.Manager()

    class Meta:
        unique_together = index_together = ('pizza', 'pizza_type', 'no_of_toppings')

    def __str__(self):
        return 'Pizza: %s - Type: %s - No of Toppings: %s' % (
            self.pizza,
            self.pizza_type,
            self.no_of_toppings
        )


class PizzaOrder(AbstractTimeStampMixin):
    uid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    pizza = models.ForeignKey(Pizza, related_name='pizza_orders', on_delete=models.CASCADE)
    pizza_type = models.CharField(
        max_length=100,
        choices=PizzaRate.PIZZA_TYPE_CHOICES,
        default=PizzaRate.SMALL
    )
    toppings = models.ManyToManyField(Topping, related_name='pizza_orders')

    def __str__(self):
        return 'UID: %s - Pizza: %s - Type: %s - Toppings: %s' % (
            self.uid,
            self.pizza,
            self.pizza_type,
            ', '.join(self.toppings.all().values_list('name', flat=True))
        )


class Sub(AbstractTimeStampMixin):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from="name", always_update=False, unique=True, db_index=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class SubRate(AbstractTimeStampMixin):
    SMALL = 'small'
    LARGE = 'large'
    SUB_TYPE_CHOICES = (
        (SMALL, 'Small'),
        (LARGE, 'Large')
    )
    sub = models.ForeignKey(Sub, related_name='sub_rates', on_delete=models.CASCADE)
    topping = models.ForeignKey(Topping, related_name='sub_rates', null=True, on_delete=models.CASCADE)
    sub_type = models.CharField(max_length=100, choices=SUB_TYPE_CHOICES, default=SMALL)
    rate = models.DecimalField(max_digits=5, decimal_places=2)
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = index_together = ('sub', 'topping', 'sub_type')

    def __str__(self):
        return 'Sub: %s - Type: %s - Topping: %s' % (
            self.sub,
            self.sub_type,
            self.topping
        )


class SubOrder(AbstractTimeStampMixin):
    uid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    sub = models.ForeignKey(Sub, related_name='sub_orders', on_delete=models.CASCADE)
    sub_type = models.CharField(max_length=100, choices=SubRate.SUB_TYPE_CHOICES, default=SubRate.SMALL)
    toppings = models.ManyToManyField(Topping, related_name='sub_orders')

    def __str__(self):
        return 'UID: %s - Sub: %s - Type: %s - Toppings: %s' % (
            self.uid,
            self.sub,
            self.sub_type,
            ', '.join(self.toppings.all().values_list('name', flat=True))
        )


class Pasta(AbstractTimeStampMixin):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from="name", always_update=False, unique=True, db_index=True)
    description = models.TextField(blank=True)
    rate = models.DecimalField(max_digits=5, decimal_places=2)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Salad(AbstractTimeStampMixin):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from="name", always_update=False, unique=True, db_index=True)
    description = models.TextField(blank=True)
    rate = models.DecimalField(max_digits=5, decimal_places=2)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class DinnerPlatter(AbstractTimeStampMixin):
    name = models.CharField(max_length=100)
    slug = AutoSlugField(populate_from="name", always_update=False, unique=True, db_index=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class PlatterRate(AbstractTimeStampMixin):
    SMALL = 'small'
    LARGE = 'large'
    PLATTER_TYPE_CHOICES = (
        (SMALL, 'Small'),
        (LARGE, 'Large')
    )
    platter = models.ForeignKey(DinnerPlatter, related_name='platter_rates', on_delete=models.CASCADE)
    platter_type = models.CharField(max_length=100, choices=PLATTER_TYPE_CHOICES, default=SMALL)
    rate = models.DecimalField(max_digits=5, decimal_places=2)
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = index_together = ('platter', 'platter_type')

    def __str__(self):
        return 'Dinner Platter: %s - Type: %s' % (
            self.platter,
            self.platter_type,
        )


class PlatterOrder(AbstractTimeStampMixin):
    uid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    platter = models.ForeignKey(DinnerPlatter, related_name='platter_orders', on_delete=models.CASCADE)
    platter_type = models.CharField(
        max_length=100,
        choices=PlatterRate.PLATTER_TYPE_CHOICES,
        default=PlatterRate.SMALL
    )

    def __str__(self):
        return 'UID: %s - Platter: %s - Type: %s' % (
            self.uid,
            self.platter,
            self.platter_type,
        )


class Order(AbstractTimeStampMixin):
    uid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    pizzas = models.ManyToManyField(PizzaOrder, related_name='orders')
    subs = models.ManyToManyField(SubOrder, related_name='orders')
    pastas = models.ManyToManyField(Pasta, related_name='orders')
    salads = models.ManyToManyField(Salad, related_name='orders')
    platters = models.ManyToManyField(PlatterOrder, related_name='orders')

    def __str__(self):
        return 'Order: %s' % (self.uid)
