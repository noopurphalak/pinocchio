from rest_framework import viewsets

from cart import models as cart_models
from cart import serializers as cart_serializers


class PizzaViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = cart_models.Pizza.objects.all().order_by("name")
    serializer_class = cart_serializers.PizzaSerializer
    lookup_field = "slug"


class ToppingViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = cart_models.Topping.objects.all().order_by("name")
    serializer_class = cart_serializers.ToppingSerializer
    lookup_field = "slug"


class PastaViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = cart_models.Pasta.objects.all().order_by("name")
    serializer_class = cart_serializers.PastaSerializer
    lookup_field = "slug"


class SaladViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = cart_models.Salad.objects.all().order_by("name")
    serializer_class = cart_serializers.SaladSerializer
    lookup_field = "slug"


class PlatterViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = cart_models.DinnerPlatter.objects.all().order_by("name")
    serializer_class = cart_serializers.PlatterSerializer
    lookup_field = "slug"


class PizzaRateViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = cart_models.PizzaRate.objects.all().order_by(
        "pizza__name", "-pizza_type", "no_of_toppings"
    )
    serializer_class = cart_serializers.PizzaRateSerializer


class PlatterRateViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = cart_models.PlatterRate.objects.all().order_by("id")
    serializer_class = cart_serializers.PlatterRateSerializer
