from cart import models as cart_models
from rest_framework import serializers


class PizzaSerializer(serializers.ModelSerializer):
    class Meta:
        model = cart_models.Pizza
        fields = ('name', 'slug', 'description')


class ToppingSerializer(serializers.ModelSerializer):
    class Meta:
        model = cart_models.Topping
        fields = ('name', 'slug')


class PastaSerializer(serializers.ModelSerializer):
    class Meta:
        model = cart_models.Pasta
        fields = ('name', 'slug', 'description', 'rate')


class SaladSerializer(serializers.ModelSerializer):
    class Meta:
        model = cart_models.Salad
        fields = ('name', 'slug', 'description', 'rate')


class PlatterSerializer(serializers.ModelSerializer):
    class Meta:
        model = cart_models.DinnerPlatter
        fields = ('name', 'slug', 'description')


class PizzaFieldSerializer(serializers.Serializer):
    def to_representation(self, instance):
        return {'name': instance.pizza.name, 'slug': instance.pizza.slug}


class PlatterFieldSerializer(serializers.Serializer):
    def to_representation(self, instance):
        return {'name': instance.platter.name, 'slug': instance.platter.slug}


class PizzaRateSerializer(serializers.ModelSerializer):
    pizza = PizzaFieldSerializer(source='*')
    class Meta:
        model = cart_models.PizzaRate
        fields = ('id', 'pizza', 'pizza_type', 'no_of_toppings', 'rate')


class PlatterRateSerializer(serializers.ModelSerializer):
    platter = PlatterFieldSerializer(source='*')

    class Meta:
        model = cart_models.PlatterRate
        fields = ('id', 'platter', 'platter_type', 'rate')
