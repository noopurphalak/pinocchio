import Vuex from 'vuex'
import Menu from './menu.store'

const createStore = () => {
    return new Vuex.Store({
        state: () => ({}),
        modules: {
            menu: Menu,
        },
        mutations: {},
        actions: {
            async nuxtServerInit(vuexContext, context) {
                const response = await context.app.$axios.$get(
                    '/cart/pizzarates/',
                )
                vuexContext.commit('setPizzaRates', response.results)
            },
        },
    })
}

export default createStore
