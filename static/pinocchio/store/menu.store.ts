import { Module, VuexModule, Mutation } from 'vuex-module-decorators'
import { IPizzaRate } from '~/interfacepool'

@Module({
    name: 'Menu',
    stateFactory: true,
    namespaced: false,
})
export default class Menu extends VuexModule {
    pizzaRates: IPizzaRate[] = []

    @Mutation
    setPizzaRates(rates: IPizzaRate[]) {
        this.pizzaRates = rates
    }
}
