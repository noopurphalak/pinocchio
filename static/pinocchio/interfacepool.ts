export type IPizzaType = 'small' | 'large'

export interface IPizza {
    name: string
    slug: string
}

export interface IPizzaRate {
    id: number
    pizza: IPizza
    pizza_type: IPizzaType
    no_of_toppings: number
    rate: number
}
